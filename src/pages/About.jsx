

const About = () => {
  return (
    <>
    <div className="flex flex-wrap gap-2 sm:gap-x-6 items-center justify-center">
        <h1 className="text-4xl font-bold leading-one tracking-tight sm:text-6xl">
            We love
        </h1>
        <div className="stats bg-primary shadow">
            <div className="stat">
                <div className="stat-title text-primary-content text-4xl font-bold tracking-widest">
                    furniture
                </div>
            </div>
        </div>
    </div>
    <div className="mt-6 text-lg leading-8 max-w-2xl mx-auto">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque omnis sequi voluptatibus adipisci consequatur, accusantium quas quidem voluptatem, autem, modi repellendus soluta consequuntur delectus tempore aperiam suscipit esse in aut. Minima ipsam ipsa vero accusamus hic, suscipit impedit saepe! Id veniam accusantium, cupiditate ab modi cumque odio porro tempore laborum!
    </div>
    </>
  )
};
export default About;